from unittest import TestCase
from homebots import *
import numpy


class TestHomeBots(TestCase):
    ''' Test stuff. '''

    def test_random_agents(self):
      agents = Agents.make_random_agents(10,10)
      self.assertEqual(len(agents), 10)
      for a in agents:
        self.assertTrue(a(0.0) >= 0 and a(0.0) <= 1.0)

    def test_agents_welfare(self):
      agents = Agents.make_random_agents(10,10)
      tv = numpy.random.rand(len(agents))
      w = agents.welfare(tv)
      wv = agents(tv)
      self.assertTrue(w == wv.sum())

    def test_value(self):
      agents = Agents.make_random_agents(10,10)
      self.assertEqual(len(agents.value()), 10)
      self.assertTrue(not agents.value().any())
