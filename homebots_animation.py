''' Un-constrained optimization test '''
import sys
import re
import scipy.optimize
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation, Animation
from random import random
from homebots import *
from tests.testbots import settings, settings_cz


def init():
  global apps, linear_agent
  if strategy == 'proxy':
    linear_agent = Agent((0,0,0,-1), (0,10))
    apps += [linear_agent]


def update_agents(frame):
  global apps, linear_agent
  ''' Mutate global or agent(s) settings for the tick '''
  if strategy == 'proxy':
    linear_agent.params = (0,0,0,-frame*0.1) # HomeBots control strategy #2
  else:
    apps.r_max = numpy.sin(frame)*14 # HomeBots control strategy #0 - just change the max!


def solve_agents(apps):
  global maxiter, ftol
  return scipy.optimize.minimize(lambda r: -1*apps(r), apps.value(), method='SLSQP',
    jac=lambda r: -1*apps.deriv(r),
    options = {
      'ftol': ftol,
      'disp': True,
      'maxiter': maxiter
    },
    constraints=apps.constraints(),
    bounds=apps.bounds
  )


def update(frame):
  print('Frame %s' % frame)
  update_agents(frame)
  o = solve_agents(apps)
  # Draw
  plt.clf()
  plt.axvline(apps.r_max, lw=1.4, color='k')
  for i,a in enumerate(apps):
    x_min = a.bounds[0] if a.bounds[0] != None else 0
    x_max = a.bounds[1] if a.bounds[1] != None else 10
    x_axis = numpy.linspace(x_min, x_max, 1000)
    o.x = numpy.fmax(o.x, numpy.zeros(len(apps))) # Can jilter slightly <0 and screws up plot
    plt.plot(x_axis, numpy.vectorize(a)(x_axis), o.x[i], a(o.x[i]), 'o')
  plt.ylim(0,3)
  plt.xlim(0,10)


if len(sys.argv) <= 1 and len(sys.argv) not in ['rmax', 'proxy']:
  raise RuntimeError("Usage: " + sys.argv[0] + " (rmax|proxy)")
strategy = sys.argv[1]

mpl.rcParams['axes.color_cycle'] = [
  'red',
  'orange',
  'yellow',
  'purple',
  'fuchsia',
  'lime',
  'green',
  'blue',
  'navy',
  'black'
]
fig, ax = plt.subplots()
ln, = plt.plot([], [], 'ro', animated=True)
apps = Agents.from_list(settings_cz)
maxiter = 12 # Optimizer setting.
ftol = 10e-6 # Optimizer setting.

ani = FuncAnimation(fig, update, frames=numpy.linspace(0,math.pi,20),
                    init_func=init, blit=True, interval=340, repeat=True)
f = re.sub('.py$', ('_%s.mp4' % strategy), __file__)
ani.save(f, writer = 'avconv')
