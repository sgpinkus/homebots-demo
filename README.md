# HomeBots Demo
HomeBots is a single shot utility curve based resource allocation method, based on global optimization. HomeBots was first described in a paper from 1996. This repo holds a simple demonstration of the dynamics of HomeBots resource allocation. The demonstration does not use exactly the same methods as described in the original paper.

# Installation
To run the Python3 scripts you need Python3 and some dependencies installed. Dependencies are listed in `requirements.txt` file. They can be installed with `pip` (>=v3) or via your package manager. To install with `pip` in \*NIX environment run the following:

    pip3 install -U pip
    pip3 install -U -r requirements.txt

# Usage

    python3 homebots_animation.py  rmax|proxy
