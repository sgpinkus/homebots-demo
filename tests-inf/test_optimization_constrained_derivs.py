''' Shows necessary condition for constrained optimal is satisfied - all derives equal. Sanity check. '''
import sys
import matplotlib.pyplot as plt
import scipy.optimize
from homebots import *
from testbots import settings, settings_cz

apps = Agents.from_list(settings_cz)
r0 = numpy.zeros(len(apps))
cons = apps.constraints()
bounds = [a.bounds for a in apps]

# Without var bounds deriv should be equal
for m in range(1,20,2):
  apps._r_max = m
  o = scipy.optimize.minimize(lambda r: -1*apps(r), r0, method='SLSQP',
    jac=lambda r: -1*apps.deriv(r),
    options = {
      'ftol': 1e-8,
      'disp': True,
      'maxiter': 1e3
    },
    constraints=cons
  )
  print([a.deriv(o.x[i]) for i,a in enumerate(apps)])
