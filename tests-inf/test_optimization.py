''' Un-constrained optimization tests '''
import matplotlib.pyplot as plt
import scipy.optimize
from homebots import *
from testbots import settings, settings_cz

# Welfare is separable so unconstrained max can actually be easily solved as N indep problems.
# Works with nelder-mead
apps = Agents.from_list(settings)
x_axis = numpy.linspace(0, 5, 1000)
for a in apps:
  o = scipy.optimize.minimize(lambda r: -1*a(r), 0, method='nelder-mead', options = {'xtol': 1e-8 , 'disp': True})
  plt.plot(x_axis, numpy.vectorize(a)(x_axis), o.x, a(o.x), '|', markersize=12, markeredgewidth=2.0)
plt.show()
# Work better with BFGS
for a in apps:
  o = scipy.optimize.minimize(lambda r: -1*a(r), 0, method='BFGS', jac=lambda r: numpy.array([-1*a.deriv(r)]), options = {'xtol': 1e-8, 'disp': True})
  plt.plot(x_axis, numpy.vectorize(a)(x_axis), o.x, a(o.x), '|', markersize=12, markeredgewidth=2.0)
plt.show()
# The equivalent but using a vector input works with BFGS.
r0 = numpy.zeros(len(apps))
o = scipy.optimize.minimize(lambda r: -1*apps(r), r0, method='BFGS', jac=lambda r: -1*apps.deriv(r), options = {'xtol': 1e-8, 'disp': True})
for i,a in enumerate(apps):
  plt.plot(x_axis, numpy.vectorize(a)(x_axis), o.x[i], a(o.x[i]), '|', markersize=12, markeredgewidth=2.0)
plt.show()
# Won't work with nelder-mead. Can't find conservative settings that will!
o = scipy.optimize.minimize(lambda r: -1*apps(r), r0, method='nelder-mead', options = {'xatol': 1e-5, 'disp': True, 'maxiter': 10e6})
for i,a in enumerate(apps):
  plt.plot(x_axis, numpy.vectorize(a)(x_axis), o.x[i], a(o.x[i]), '|', markersize=12, markeredgewidth=2.0)
plt.show()
