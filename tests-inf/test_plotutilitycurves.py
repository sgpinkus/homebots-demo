import matplotlib.pyplot as plt
import scipy.optimize
from homebots import *

apps = Agents.make_random_agents(20, 10)
''' Basics '''
r0 = numpy.zeros(len(apps))
print(apps.u(r0))
print(apps.welfare(r0))
''' Pretend welfare is a 1D function and optimize it '''
_welfare = lambda r: apps.welfare(numpy.ones(len(apps))*r)
o_x = scipy.optimize.fmin(lambda r: -1*_welfare(r), 0)
x_axis = numpy.linspace(min(0,o_x-1), max(3,o_x+1), 1000)
plt.plot(x_axis, numpy.vectorize(_welfare)(x_axis), o_x, _welfare(o_x), 'or')
for a in apps:
  plt.plot(x_axis, numpy.vectorize(a)(x_axis))
''' Add random point on each utility function for test. '''
rR = numpy.random.rand(len(apps))*3
rV = apps.u(rR)
plt.plot(rR,rV,'og')
plt.show()
