import matplotlib.pyplot as plt
import scipy.optimize
from homebots import *


apps = Agents.make_random_agents(10, 10)
_welfare = lambda r: apps.welfare(numpy.ones(len(apps))*r)
o = scipy.optimize.minimize(lambda r: -1*_welfare(r), 0, method='nelder-mead', options={'xtol': 1e-6, 'disp': True})
x_axis = numpy.linspace(min(0,o.x-1), max(3,o.x+1), 1000)
plt.plot(x_axis, numpy.vectorize(_welfare)(x_axis), o.x, _welfare(o.x), 'or')
plt.show()
