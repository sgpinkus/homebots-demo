import numpy
import matplotlib.pyplot as plt
import scipy.optimize
import random
import math
from functools import reduce


class Agent:
  '''
  Represents an agent/consumer. Holds utility curve settings and resource low/high bounds.
  Also holds a current value. The current value holds the result of some negotiation mechanism - I.e. HomeBots.
  '''
  _bounds = (0.,None) # Don't allow -ve.
  _params = (0.,0.,0.,0.)
  _value = 0

  def __init__(self, params, bounds=(None, None), value=0):
    self.bounds = bounds
    self.params = params
    self.value = value

  def __call__(self, r=None):
    r = self.value if r == None else r
    ''' Calling the agent with a scalar returns the utility. See u().'''
    return self.u(r)

  def u(self, r=None):
    ''' The value of the utility function at `r`. '''
    r = self.value if r == None else r
    return Agent._u(r, *self.params)

  def deriv(self, r=None):
    ''' The value of derivative of utility function at `r`. '''
    r = self.value if r == None else r
    return Agent._agentseriv(r, *self.params[1:])

  @property
  def params(self):
    return self._params

  @params.setter
  def params(self, params):
    (a,b,c,d) = params
    self._params = (a, b, c, d)

  @property
  def bounds(self):
    ''' Lower uppoer bound constraints on resource consumption i.e. the domain of u(). '''
    return self._bounds

  @bounds.setter
  def bounds(self, bounds):
    (r_min, r_max) = bounds
    if r_min != None and r_max != None and r_min >= r_max:
      raise ValueError()
    if r_min != None and r_min < 0:
      raise ValueError()
    if r_max != None and r_max <= 0:
      raise ValueError()
    self._bounds = (r_min, r_max)

  @property
  def value(self):
    return self._value

  @value.setter
  def value(self, value):
    self._value = value

  def tolist(self):
    ''' Pack up as a list. List can be passed to constructor to re-init. '''
    return (self.params, self.bounds)

  @classmethod
  def _u(cls, r, a, b, c, d):
    ''' The general form of the HomeBots utility functions. '''
    return a - b * math.exp(-1.0*c*r) - (d*r)

  @classmethod
  def _agentseriv(cls, r, b, c, d):
    ''' The general form of the derivative of _u() function. '''
    return b * c * math.exp(-1*c*r) - d

  @classmethod
  def make_random_utility_params(cls, num=10):
    ''' Creates homebot utility function params with desirable properties:
      - 0 <= u(0) <= 0.5
      - u`(0) > 0
    Note u(0) = a - b, and u`(0) = bc - d
    Returns as a `num` by 4 2d ndarray.
    '''
    a=numpy.random.rand(num)*0.5+0.5 # Between
    b=numpy.fmin(numpy.random.rand(num)*0.2+0.8, a)
    c=numpy.random.rand(num)*0.2+0.8
    d=numpy.fmin(numpy.random.rand(num)*0.5, b*c*0.5)
    return numpy.stack((a,b,c,d)).transpose()

  @classmethod
  def make_random_agents(cls, num=10):
    ''' Creates homebot utility function with desirable params. See make_homebot_utility_params(). '''
    prms = Agent.make_random_utility_params(num)
    return [Agent(p) for p in prms[:]]


class Agents:
  '''
  A container for a collection of agents.
  Contains the objective function @welfare, the max global resource @r_max and some other things for convenience.
  '''
  _agents = [] # list of agents.
  _r_max  = None   # max system resource available.

  def __init__(self, agents, r_max):
    ''' Init. Don't use this directly. Use factories make_xxx(). '''
    self.agents = agents
    self.r_max = r_max


  def __iter__(self):
    return self.agents.__iter__()

  def __call__(self, r):
    return self.welfare(r)

  def __len__(self):
    return len(self.agents)

  def __getitem__(self, key):
    return self._agents[key]

  def __setitem__(self, key, value):
    self._agents[key] = value

  def __delitem__(self, key):
    print(key, len(self), len(self._agents))
    del(self._agents[key])

  def __iter__(self):
    return self._agents.__iter__()

  def __contains__(self, item):
    return item in self._agents

  def __iadd__(self, other):
    self._agents += other
    return self

  def u(self, r):
    ''' Calculate the utility of `r` as a len(self) dim array of individual utility scores.
    welfare() just sums this vector up and returns a scalar.
    '''
    self._check_agentdomain(r)
    return numpy.array([self.agents[i](r[i]) for i in range(0,len(r))])

  def deriv(self, r):
    self._check_agentdomain(r)
    return numpy.array([self.agents[i].deriv(r[i]) for i in range(0,len(r))])

  def welfare(self, r):
    ''' Just sums up self.u(r) to give global welfare for agents at `r`. `r` is a vector of len len(self). '''
    return sum(self.u(r))

  def value(self):
    return numpy.array([self.agents[i].value for i in range(0,len(self))])

  @property
  def r_max(self):
    return self._r_max

  @r_max.setter
  def r_max(self, r_max):
    self._r_max = r_max

  @property
  def agents(self):
    return self._agents

  @agents.setter
  def agents(self, agents):
    self._agents = agents

  @property
  def bounds(self):
    return [a.bounds for a in self.agents]

  def tolist(self):
    l = [self._r_max]
    l += [list(map(lambda app: app.tolist(), self.agents))]
    return l

  def constraints(self):
    constraints = ({'type': 'eq', 'fun' : lambda r: self._r_max - r.sum()},)
    return constraints

  def _check_agentdomain(self, r):
    if len(r) != len(self):
      raise ValueError()

  @classmethod
  def make_random_agents(cls, num, r_max):
    return Agents(Agent.make_random_agents(num), r_max)

  @classmethod
  def from_list(cls, _list):
    _apps = []
    for app in _list[1]:
      _apps.append(Agent(*app))
    return Agents(_apps, _list[0])
